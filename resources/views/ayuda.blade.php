<x-layout>
    <x-slot:titulo>Ayuda</x-slot>
    <div class="row">
        <h1>Ayuda</h1>
    </div>
    <div class="row">
        <x-button :href="route('inicio')" texto="volver" clase="btn btn-primary"></x-button>
    </div>
    <div class="row">
        <x-formulario tipo="uno" texto="-"></x-formulario>
    </div>
    <div class="row">
        <x-formulario tipo="dos"></x-formulario>
    </div>
</x-layout>
