<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo', 'Ejemplo de clase')</title>
    @vite(['resources/css/app.scss'])
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Mensajes</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('inicio') ? 'active' : '' }}" aria-current="page"
                                href="{{ route('inicio') }}">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('mensajes.index') ? 'active' : '' }}"
                                href="{{ route('mensajes.index') }}">Mensajes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('mensajes.create') ? 'active' : '' }}"
                                href="{{ route('mensajes.create') }}">Crear mensaje</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('ayuda') ? 'active' : '' }}"
                                href="{{ route('ayuda') }}">Ayuda</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <div class="row">
            @yield('contenido')
        </div>
    </div>
</body>
@vite(['resources/js/app.js'])

</html>
