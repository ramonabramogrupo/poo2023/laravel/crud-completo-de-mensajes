@if ($attributes['tipo'] == 'uno')
    <div>
        Formulario de ayuda usuario registrado {{ $texto }}
    </div>
@else
    <div>
        Formulario de ayuda usuario no registrado {{ $texto }}
    </div>
@endif
