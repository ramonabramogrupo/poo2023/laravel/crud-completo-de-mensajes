@extends('layouts.main')

@section('titulo', 'Ver')

@section('contenido')
    <div class="row">
        <h1>Mostrar mensaje {{ $mensaje->id }}</h1>
    </div>
    <div class="row">
        <div class="m-1">
            <div class="card" style="min-height: 200px">
                <div class="card-body">
                    <h5 class="card-title">Autor: {{ $mensaje->autor }}</h5>
                    <p class="card-text">{{ $mensaje->texto }}</p>
                </div>
                <div class="vertical-align-bottom d-flex justify-content-end">
                    <form action="{{ route('mensajes.destroy', $mensaje) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger m-1">Eliminar</button>
                    </form>
                    <a href="{{ route('mensajes.edit', $mensaje->id) }}" class="btn btn-primary m-1">Actualizar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="alert alert-sucess">
            {{ Session::get('informacion') }}
            {{ session('informacion') }}
        </div>
    </div>

@endsection
