@extends('layouts.main')

@section('titulo', 'Creando')

@section('contenido')
    <div class="row">
        <h1>Nuevo Mensaje</h1>
    </div>
    <div class="container h-100 mt-5">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-10 col-md-8 col-lg-6">
                <h3>Añadir un mensaje</h3>
                <form action="{{ route('mensajes.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="texto">Autor</label>
                        <input type="text" class="form-control" id="autor" name="autor"><br>
                        @error('autor')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label for="body">Texto</label>
                        <textarea class="form-control" id="texto" name="texto" rows="3"></textarea><br>
                        @error('texto')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Añadir</button>
                </form>
            </div>
        </div>
    </div>

@endsection
