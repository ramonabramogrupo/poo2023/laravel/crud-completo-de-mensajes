@extends('layouts.main')

@section('titulo', 'Listado')

@section('contenido')
    <div class="row">
        <h1>Listado de mensajes</h1>
    </div>

    <div class="row mt-3">
        <div class="alert alert-sucess">
            {{ session('informacion') }}
        </div>
    </div>

    <div class="row">
        @foreach ($mensajes as $mensaje)
            <div class="col-lg-5 m-1">
                <div class="card" style="min-height: 200px">
                    <div class="card-body">
                        <h5 class="card-title">Autor: {{ $mensaje->autor }}</h5>
                        <p class="card-text">{{ $mensaje->texto }}</p>
                    </div>
                    <div class="vertical-align-bottom d-flex justify-content-end">
                        <form action="{{ route('mensajes.destroy', $mensaje) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger m-1">Eliminar</button>
                        </form>
                        <a href="{{ route('mensajes.show', $mensaje->id) }}" class="btn btn-primary m-1">Ver</a>
                        <a href="{{ route('mensajes.edit', $mensaje->id) }}" class="btn btn-primary m-1">Actualizar</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="d-flex justify-content-end mt-3">
            {{ $mensajes->links() }}
        </div>
    </div>
@endsection
