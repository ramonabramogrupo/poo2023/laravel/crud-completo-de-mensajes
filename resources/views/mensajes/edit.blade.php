@extends('layouts.main')

@section('titulo', 'Editando')

@section('contenido')
    <div class="row">
        <h1>Editando Mensaje</h1>
    </div>
    <div class="container h-100 mt-5">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-10 col-md-8 col-lg-6">
                <h3>Editando un mensaje</h3>
                <form action="{{ route('mensajes.update', $mensaje) }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="texto">Autor</label>
                        <input type="text" class="form-control" id="autor" name="autor" value="{{ $mensaje->autor }}"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="body">Texto</label>
                        <textarea class="form-control" id="texto" name="texto" rows="3" required>{{ $mensaje->texto }}</textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </form>
            </div>
        </div>
    </div>

@endsection
