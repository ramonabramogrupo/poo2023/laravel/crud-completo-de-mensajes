<?php

namespace App\Http\Controllers;

use App\Models\Mensaje;

use Illuminate\Http\Request;

class MensajeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mensaje = Mensaje::paginate(6);
        // return $mensaje; // para probar
        return view('mensajes.index', [
            'mensajes' => $mensaje
        ]);
    }
    public function show(int $id)
    {
        return view('mensajes.ver', [
            'mensaje' => Mensaje::find($id),
        ]);
    }

    public function create()
    {
        return view('mensajes.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'texto' => 'required|max:100|min:10',
            'autor' => 'required|min:3',
        ]);
        $mensaje = Mensaje::create($request->all());
        // $mensaje = new Mensaje();
        // $mensaje->texto = $request->input('texto');
        // $mensaje->autor = $request->input('autor');
        // $mensaje->save();

        return redirect()->route('mensajes.show', $mensaje->id)
            ->with('informacion', 'Mensaje creado correctamente');
    }

    public function edit(int $id)
    {
        $mensaje = Mensaje::find($id);
        return view('mensajes.edit', [
            'mensaje' => $mensaje
        ]);
    }
    public function update(Request $request, Mensaje $mensaje)
    {
        $request->validate([
            'texto' => 'required',
            'autor' => 'required',
        ]);

        $mensaje->update($request->all());


        return redirect()->route('mensajes.show', $mensaje->id)
            ->with('informacion', 'Mensaje actualizado correctamente');
    }


    public function destroy(Mensaje $mensaje)
    {
        $mensaje->delete();
        return redirect()->route('mensajes.index')
            ->with('informacion', 'Mensaje eliminado correctamente');
    }
}
