<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Button extends Component
{
    public $texto;
    public $href;
    public $clase;

    /**
     * Create a new component instance.
     */
    public function __construct($texto, $href, $clase)
    {
        $this->texto = $texto;
        $this->href = $href;
        $this->clase = $clase;
    }


    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.button');
    }
}
