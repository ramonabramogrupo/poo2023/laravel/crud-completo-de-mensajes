<?php

use App\Http\Controllers\MensajeController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
})->name('inicio');

Route::view('/ayuda', 'ayuda')->name('ayuda');


Route::controller(MensajeController::class)->group(function () {
    Route::prefix('mensajes')->group(function () {
        Route::get('/', 'index')->name('mensajes.index');
        Route::get('/index', 'index')->name('mensajes.index');
        Route::get('/show/{id}', 'ver')->name('mensajes.show');
        Route::get('/create', 'create')->name('mensajes.create');
        Route::post('/', 'store')->name('mensajes.store');
        Route::get('/edit/{id}', 'edit')->name('mensajes.edit');
        Route::put('/update/{mensaje}', 'update')->name('mensajes.update');
        Route::delete('/destroy/{mensaje}', 'destroy')->name('mensajes.destroy');
    });
});


Route::resource('mensajes', MensajeController::class);